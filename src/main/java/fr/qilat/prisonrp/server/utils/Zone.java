package fr.qilat.prisonrp.server.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.server.zonemanager.ZoneManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.awt.*;


/**
 * Created by Qilat on 29/11/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Zone {
    private int id;
    private String name;
    private String worldName;
    private int pos1X;
    private int pos1Y;
    private int pos1Z;
    private int pos2X;
    private int pos2Y;
    private int pos2Z;
    private State state;

    public Zone() {
    }

    public Zone(World world, BlockPos pos1, BlockPos pos2) {
        this(null, null, world, pos1, pos2);
    }

    public Zone(ZoneManager manager, String name, World world, BlockPos pos1, BlockPos pos2) {
        this.id = manager != null ? manager.getNextId() : 0;
        this.name = name;
        this.worldName = world.getWorldInfo().getWorldName();
        this.setPos1(pos1);
        this.setPos2(pos2);
        this.state = State.ENABLE;
    }

    public boolean contains(Location loc) {
        int posX = loc.getBlockPos().getX();
        int posZ = loc.getBlockPos().getZ();
        return ((posX <= pos1X && posX >= pos2X)
                || (posX >= pos1X && posX <= pos2X))
                && ((posZ <= pos1Z && posZ >= pos2Z)
                || (posZ >= pos1Z && posZ <= pos2Z))
                && loc.getWorld().getWorldInfo().getWorldName().equals(this.worldName);
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setPos1(BlockPos pos1) {
        this.pos1X = pos1.getX();
        this.pos1Y = pos1.getY();
        this.pos1Z = pos1.getZ();
    }

    public void setPos2(BlockPos pos2) {
        this.pos2X = pos2.getX();
        this.pos2Y = pos2.getY();
        this.pos2Z = pos2.getZ();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPos1X() {
        return pos1X;
    }

    public void setPos1X(int pos1X) {
        this.pos1X = pos1X;
    }

    public int getPos1Y() {
        return pos1Y;
    }

    public void setPos1Y(int pos1Y) {
        this.pos1Y = pos1Y;
    }

    public int getPos1Z() {
        return pos1Z;
    }

    public void setPos1Z(int pos1Z) {
        this.pos1Z = pos1Z;
    }

    public int getPos2X() {
        return pos2X;
    }

    public void setPos2X(int pos2X) {
        this.pos2X = pos2X;
    }

    public int getPos2Y() {
        return pos2Y;
    }

    public void setPos2Y(int pos2Y) {
        this.pos2Y = pos2Y;
    }

    public int getPos2Z() {
        return pos2Z;
    }

    public void setPos2Z(int pos2Z) {
        this.pos2Z = pos2Z;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }


    public BlockPos getZoneCenter() {
        return new BlockPos((getPos2X() + getPos1X()) / 2, (getPos2Y() + getPos1Y()) / 2, (getPos2Z() + getPos1Z()) / 2);
    }

    public String getWorldName() {
        return worldName;
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }


    public enum State {
        ENABLE("prisonrp.btt.statesfz.enable", new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/enableEntry.png")),
        DISABLE("prisonrp.btt.statesfz.disable", new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/disableEntry.png")),
        DELETED("prisonrp.btt.statesfz.delete", new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/deletedEntry.png"));

        String langPathName;
        ResourceLocation background;

        State(String pathName, ResourceLocation background) {
            this.langPathName = pathName;
            this.background = background;
        }

        public ResourceLocation getBackground() {
            return this.background;
        }
        public String getPathName() {
            return this.langPathName;
        }
    }

}
