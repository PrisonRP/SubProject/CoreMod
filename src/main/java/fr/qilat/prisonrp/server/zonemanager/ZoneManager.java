package fr.qilat.prisonrp.server.zonemanager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.server.utils.Location;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Qilat on 11/12/2017 for PrisonRP.
 */
public abstract class ZoneManager {
    protected ZoneManager instance;
    private File saveFile;
    private int currentId = 0;
    private ArrayList<Zone> zones = new ArrayList<Zone>();

    public void load() {
        try {
            saveFile = new File(PrisonRPCore.configDirectory, getSaveFileName());
            if (saveFile.exists())
                zones = new ArrayList<Zone>(Arrays.asList(new ObjectMapper().readValue(saveFile, Zone[].class)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setupCurrentID();

    }

    public void setupCurrentID() {
        for (Zone zone : getZones())
            if (zone.getId() >= this.currentId)
                this.currentId = zone.getId() + 1;
    }

    public void save() {
        try {
            new ObjectMapper().writeValue(saveFile, this.zones);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isInOneZone(Location loc) {
        for (Zone zone : zones)
            if (zone.getState().equals(Zone.State.ENABLE)
                    && zone.contains(loc))
                return true;
        return false;
    }

    public ArrayList<Zone> getZones() {
        return zones;
    }

    public int create(World world, Location pos1, Location pos2) {
        return this.create(null, world,
                        pos1.getBlockPos(),
                        pos2.getBlockPos());
    }

    public int create(String name, World world, BlockPos pos1, BlockPos pos2) {
        Zone zone = new Zone(this, name, world, pos1, pos2);
        zones.add(zone);
        save();
        return zone.getId();
    }

    public boolean exist(int id) {
        for (Zone zone : zones)
            if (zone.getId() == id)
                return true;
        return false;
    }

    public boolean enable(int id) {
        Zone zone;
        if ((zone = get(id)) != null) {
            zone.setState(Zone.State.ENABLE);
            save();
            return true;
        }
        return false;
    }

    public boolean disable(int id) {
        Zone zone;
        if ((zone = get(id)) != null) {
            zone.setState(Zone.State.DISABLE);
            save();
            return true;
        }
        return false;
    }

    public boolean delete(int id) {
        Zone zone;
        if ((zone = get(id)) != null) {
            zones.remove(zone);
            save();
            return true;
        }
        return false;
    }

    public Zone get(int id) {
        for (Zone zone : zones)
            if (id == zone.getId())
                return zone;
        return null;
    }
    public int getIDinList(Zone zone) {
        for (int i = 0; i < zones.size(); i++)
            if (zones.get(i).getId() == zone.getId())
                return i;
        return -1;
    }

    public void update(Zone updated) {
        if(updated.getState().equals(Zone.State.DELETED)){
            delete(updated.getId());
        }else {
            if (get(updated.getId()) != null)
                getZones().set(getIDinList(updated), updated);
        }
    }

    public abstract String getSaveFileName();

    public int getNextId() {
        return this.currentId++;
    }
}
