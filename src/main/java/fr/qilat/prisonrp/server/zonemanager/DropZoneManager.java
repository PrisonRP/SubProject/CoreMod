package fr.qilat.prisonrp.server.zonemanager;

import fr.qilat.prisonrp.server.game.drop.Drop;
import fr.qilat.prisonrp.server.utils.Location;
import fr.qilat.prisonrp.server.utils.Zone;
import fr.qilat.prisonrp.server.utils.task.Task;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

import java.util.Random;

/**
 * Created by Qilat on 11/12/2017 for PrisonRP.
 */
public class DropZoneManager extends ZoneManager {
    private static final String SAVE_FILE_NAME = "dropzones.json";

    private static DropZoneManager instance;

    private int spawnTask;

    private DropZoneManager() {
    }

    public static void init() {
        if (instance == null) {
            instance = new DropZoneManager();
            instance.load();

            instance.spawnTask = new Task(Side.SERVER) {
                @Override
                public void run() {
                    if(DropZoneManager.get().getZones() != null && !DropZoneManager.get().getZones().isEmpty()) {
                        Zone zone = DropZoneManager.instance.getZones().get(new Random().nextInt(DropZoneManager.instance.getZones().size()));
                        new Drop(new Location(getWorld(zone.getWorldName()), zone.getPos1X(), zone.getPos1Y(), zone.getPos1Z()));
                    }
                }
            }.runTaskTimer(30 * 60 * 20L, 0L);
        }
    }

    public static DropZoneManager get() {
        return instance;
    }

    @Override
    public String getSaveFileName() {
        return SAVE_FILE_NAME;
    }

    public static World getWorld(String name){
        for(World w : DimensionManager.getWorlds()){
            if(w.getWorldInfo().getWorldName().equals(name))
                return w;
        }
        return null;
    }
}
