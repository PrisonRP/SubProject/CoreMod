package fr.qilat.prisonrp.server.zonemanager;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Qilat on 28/11/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.SERVER)
public class SafeZoneManager extends ZoneManager {
    private static final String SAVE_FILE_NAME = "safezones.json";

    private static SafeZoneManager instance;

    private SafeZoneManager() {
    }

    public static void init() {
        if (instance == null) {
            instance = new SafeZoneManager();
            instance.load();
        }
    }

    public static SafeZoneManager get() {
        return instance;
    }

    @SideOnly(Side.SERVER)
    @Override
    public String getSaveFileName() {
        return SAVE_FILE_NAME;
    }
}
