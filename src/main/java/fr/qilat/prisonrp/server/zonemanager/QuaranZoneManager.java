package fr.qilat.prisonrp.server.zonemanager;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public class QuaranZoneManager extends ZoneManager {
    private static final String SAVE_FILE_NAME = "quaranzones.json";

    private static QuaranZoneManager instance;

    private QuaranZoneManager() {
    }

    public static void init() {
        if (instance == null) {
            instance = new QuaranZoneManager();
            instance.load();
        }
    }

    public static QuaranZoneManager get() {
        return instance;
    }

    @SideOnly(Side.SERVER)
    @Override
    public String getSaveFileName() {
        return SAVE_FILE_NAME;
    }
}
