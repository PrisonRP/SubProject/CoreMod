package fr.qilat.prisonrp.server.game.jobs;

import fr.qilat.prisonrp.PrisonRPCore;
import net.minecraft.util.ResourceLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GartoxFR on 17/12/2017.
 */
public class Job {

    private static List<Job> jobs = new ArrayList<Job>();

    static {
        jobs.add(new Job(0, "Gardien", new ResourceLocation(PrisonRPCore.MODID, "textures/gui/jobs/gardien.png")));
        jobs.add(new Job(1, "Gouverneur", new ResourceLocation(PrisonRPCore.MODID, "textures/gui/jobs/gardien.png")));
        jobs.add(new Job(2, "Prisonier", new ResourceLocation(PrisonRPCore.MODID, "textures/gui/jobs/gardien.png")));
    }

    private int id;
    private String name;
    private ResourceLocation icon;

    public Job(int id, String name, ResourceLocation icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ResourceLocation getIcon() {
        return icon;
    }

    public static List<Job> getJobs() {
        return jobs;
    }

    public static Job getById(int id) {
        for (Job job : jobs) {
            if(job.getId() == id) return job;
        }

        return null;
    }
}
