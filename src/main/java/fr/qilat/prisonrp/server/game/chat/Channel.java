package fr.qilat.prisonrp.server.game.chat;

import net.minecraft.network.play.client.CPacketChatMessage;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public class Channel {

    public static final Channel ALL = new Channel(0, "TOUS");
    public static final Channel STAFF = new Channel(0, "STAFF");
    public static final Channel HRP = new Channel(1, "HRP");
    public static final Channel RP = new Channel(2, "RP");
    public static final Channel RECRUTEMENT = new Channel(3, "RECRUTEMENT");
    public static final Channel GUILDE = new Channel(4, "GUILDE");
    public static final Channel COMMERCE = new Channel(5, "COMMERCE");

    private int id;
    private String name;

    Channel(int id, String name){
        this.id = id;
        this.name = name;
    }
}
