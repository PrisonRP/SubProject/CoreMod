package fr.qilat.prisonrp.server.game.chat;

import java.util.UUID;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public class Message {
    private Channel channel;
    private UUID uuid;
    private String content;

    public Message(Channel channel, UUID uuid, String content) {
        this.channel = channel;
        this.uuid = uuid;
        this.content = content;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
