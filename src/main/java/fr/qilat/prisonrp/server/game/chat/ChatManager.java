package fr.qilat.prisonrp.server.game.chat;

import java.util.HashMap;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public class ChatManager {
    private HashMap<Channel, Message> messages = new HashMap<Channel, Message>();

    private void addMessage(Message message){
        messages.put(message.getChannel(), message);
    }
}
