package fr.qilat.prisonrp.server.commands;

import fr.qilat.prisonrp.server.game.drop.Drop;
import fr.qilat.prisonrp.server.network.packets.DropZonePacket;
import fr.qilat.prisonrp.server.network.packets.QuaranZonePacket;
import fr.qilat.prisonrp.server.network.packets.SafeZonePacket;
import fr.qilat.prisonrp.server.utils.Location;
import fr.qilat.prisonrp.server.utils.Zone;
import fr.qilat.prisonrp.server.zonemanager.DropZoneManager;
import fr.qilat.prisonrp.server.zonemanager.QuaranZoneManager;
import fr.qilat.prisonrp.server.zonemanager.SafeZoneManager;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.server.permission.PermissionAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Qilat on 03/12/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.SERVER)
public class DropCommand extends CommandBase {
    private static final String NAME = "drop";
    private static final String USAGE = "/drop <one|create|list|delete|reload>";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        for (String str : sender.getServer().getPlayerList().getOppedPlayerNames()) {
            if (str == sender.getName()) {
                return USAGE;
            }
        }
        return null;
    }

    @Override
    public List<String> getAliases() {
        return new ArrayList<String>(Arrays.asList(new String[]{NAME}));
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return !(sender instanceof EntityPlayer) || PermissionAPI.hasPermission((EntityPlayer) sender, "prisonrp.command.drop");
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length < 1)
            throw new WrongUsageException(this.getUsage(sender));
        if (!(sender instanceof EntityPlayerMP))
            throw new WrongUsageException("Impossible depuis cette entité.");

        String subCommand = args[0];

        if (subCommand.equals("one")) {
            BlockPos pos = sender.getPosition();
            new Drop(new Location(sender.getEntityWorld(), pos));

            sender.sendMessage(new TextComponentString("Vous venez de faire apparaitre un colis en " + pos + "."));
        } else if (subCommand.equals("create")) {

            if (args.length > 2)
                throw new WrongUsageException(this.getUsage(sender));
            if (PosCommand.pos1HashMap.get(sender) == null)
                throw new CommandException("Vous n'avez pas sélectionné de positions à l'aide de la commande /pos <1>.");

            Location pos1 = PosCommand.pos1HashMap.get(sender);
            Location pos2 = new Location(pos1.getWorld(), new BlockPos(-1, -1, -1));

            int id = DropZoneManager.get().create(pos1.getWorld(), pos1, pos2);

            sender.sendMessage(new TextComponentString("Vous avez créer un point de drop. ID : " + id));

        } else if (subCommand.equals("list")) {
            if (args.length != 1)
                throw new WrongUsageException(this.getUsage(sender));
            EntityPlayerMP player = (EntityPlayerMP) sender;
            DropZonePacket.sendZonesToPlayer(player.getUniqueID(), true);

        } else if (subCommand.equals("delete")) {
            if (args.length != 2)
                throw new WrongUsageException(this.getUsage(sender));
            int dropzoneID = parseInt(args[1]);
            if (DropZoneManager.get().exist(dropzoneID)) {
                DropZoneManager.get().delete(dropzoneID);
                sender.sendMessage(new TextComponentString("Vous venez de supprimer le point de drop n°" + dropzoneID + "."));
            } else {
                throw new WrongUsageException("Point de drop inexistance");
            }
        } else if (subCommand.equals("info")) {
            if (args.length != 2)
                throw new WrongUsageException(this.getUsage(sender));
            int dropzoneID = parseInt(args[1]);
            if (DropZoneManager.get().exist(dropzoneID)) {
                Zone zone = DropZoneManager.get().get(dropzoneID);
                sender.sendMessage(new TextComponentString("Position n°1 : x=" + zone.getPos1X() + " y=" + zone.getPos1Y() + " z=" + zone.getPos1Z()));
                sender.sendMessage(new TextComponentString("Position n°2 : x=" + zone.getPos2X() + " y=" + zone.getPos2Y() + " z=" + zone.getPos2Z()));
            } else {
                throw new WrongUsageException("Point de drop inexistante");
            }
        } else if (subCommand.equals("reload")) {
            if (args.length != 1)
                throw new WrongUsageException(this.getUsage(sender));
            DropZoneManager.get().load();
            sender.sendMessage(new TextComponentString("Vous venez de recharger la config des points de drop."));
        }
    }
}