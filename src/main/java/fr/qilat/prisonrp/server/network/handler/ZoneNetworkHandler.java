package fr.qilat.prisonrp.server.network.handler;

import fr.qilat.prisonrp.server.utils.Zone;

import java.util.ArrayList;

/**
 * Created by Qilat on 11/12/2017 for PrisonRP.
 */
public abstract class ZoneNetworkHandler {

    private ArrayList<Zone> zones = new ArrayList<Zone>();

    public ArrayList<Zone> getZones() {
        return zones;
    }

    public void setZones(ArrayList<Zone> zones) {
        this.zones = zones;
    }

    public abstract void loadZones();

    public abstract void zonesLoaded(boolean openGUI);

    public abstract void saveZone(Zone newSafeZone);

    public int countZone(Zone.State state){
        int i = 0;
        for(Zone z : zones)
            if(z.getState().equals(state))
                i++;
        return i;
    }

    public int countZone(){
        int i = 0;
        for(Zone ignored : zones)
                i++;
        return i;
    }

    public void deleteZone(Zone zone){
        zones.remove(zone);
    }
}
