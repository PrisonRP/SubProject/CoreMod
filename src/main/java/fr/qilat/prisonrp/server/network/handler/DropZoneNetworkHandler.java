package fr.qilat.prisonrp.server.network.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.qilat.prisonrp.client.gui.GuiDropZone;
import fr.qilat.prisonrp.server.network.PacketHandler;
import fr.qilat.prisonrp.server.network.packets.DropZonePacket;
import fr.qilat.prisonrp.server.network.packets.PacketAction;
import fr.qilat.prisonrp.server.network.packets.To;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.client.Minecraft;

/**
 * Created by Qilat on 11/12/2017 for PrisonRP.
 */
public class DropZoneNetworkHandler extends ZoneNetworkHandler {

    private static DropZoneNetworkHandler instance;

    private DropZoneNetworkHandler() {
    }

    public static void init() {
        if (instance == null) {
            instance = new DropZoneNetworkHandler();
        }
    }

    public static DropZoneNetworkHandler get() {
        return instance;
    }

    @Override
    public void loadZones() {
        PacketHandler.sendPacketToServer(new DropZonePacket(Minecraft.getMinecraft().player.getUniqueID(), PacketAction.GETLIST, To.SERVER, "", true));
    }

    @Override
    public void zonesLoaded(boolean openGUI) {
        if (openGUI)
            Minecraft.getMinecraft().displayGuiScreen(new GuiDropZone());
    }

    @Override
    public void saveZone(Zone newDropZone) {
        try {
            PacketHandler.sendPacketToServer(new DropZonePacket(Minecraft.getMinecraft().player.getUniqueID(), PacketAction.UPDATEONE, To.SERVER, new ObjectMapper().writeValueAsString(newDropZone), true));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
