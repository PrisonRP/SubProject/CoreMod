package fr.qilat.prisonrp.server.network.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.qilat.prisonrp.client.gui.GuiSfz;
import fr.qilat.prisonrp.server.network.PacketHandler;
import fr.qilat.prisonrp.server.network.packets.PacketAction;
import fr.qilat.prisonrp.server.network.packets.SafeZonePacket;
import fr.qilat.prisonrp.server.network.packets.To;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


/**
 * Created by Qilat on 06/12/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.CLIENT)
public class SafeZoneNetworkHandler extends ZoneNetworkHandler {

    private static SafeZoneNetworkHandler instance;

    private SafeZoneNetworkHandler() {
    }

    public static void init() {
        if (instance == null) {
            instance = new SafeZoneNetworkHandler();
        }
    }

    public static SafeZoneNetworkHandler get() {
        return instance;
    }

    @Override
    public void loadZones() {
        PacketHandler.sendPacketToServer(new SafeZonePacket(Minecraft.getMinecraft().player.getUniqueID(), PacketAction.GETLIST, To.SERVER, "", true));
    }

    @Override
    public void zonesLoaded(boolean openGUI) {
        if (openGUI)
            Minecraft.getMinecraft().displayGuiScreen(new GuiSfz());
    }

    @Override
    public void saveZone(Zone newSafeZone) {
        try {
            PacketHandler.sendPacketToServer(new SafeZonePacket(Minecraft.getMinecraft().player.getUniqueID(), PacketAction.UPDATEONE, To.SERVER, new ObjectMapper().writeValueAsString(newSafeZone), true));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
