package fr.qilat.prisonrp.server.network.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.qilat.prisonrp.client.gui.GuiQuaranZone;
import fr.qilat.prisonrp.client.gui.GuiSfz;
import fr.qilat.prisonrp.server.network.PacketHandler;
import fr.qilat.prisonrp.server.network.packets.PacketAction;
import fr.qilat.prisonrp.server.network.packets.QuaranZonePacket;
import fr.qilat.prisonrp.server.network.packets.SafeZonePacket;
import fr.qilat.prisonrp.server.network.packets.To;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
@SideOnly(Side.CLIENT)
public class QuaranZoneNetworkHandler extends ZoneNetworkHandler{
    private static QuaranZoneNetworkHandler instance;

    private QuaranZoneNetworkHandler() {
    }

    public static void init() {
        if (instance == null) {
            instance = new QuaranZoneNetworkHandler();
        }
    }

    public static QuaranZoneNetworkHandler get() {
        return instance;
    }

    @Override
    public void loadZones() {
        PacketHandler.sendPacketToServer(new QuaranZonePacket(Minecraft.getMinecraft().player.getUniqueID(), PacketAction.GETLIST, To.SERVER, "", true));
    }

    @Override
    public void zonesLoaded(boolean openGUI) {
        if (openGUI)
            Minecraft.getMinecraft().displayGuiScreen(new GuiQuaranZone());
    }

    @Override
    public void saveZone(Zone newSafeZone) {
        try {
            PacketHandler.sendPacketToServer(new QuaranZonePacket(Minecraft.getMinecraft().player.getUniqueID(), PacketAction.UPDATEONE, To.SERVER, new ObjectMapper().writeValueAsString(newSafeZone), true));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}

