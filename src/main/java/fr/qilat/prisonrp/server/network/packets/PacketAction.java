package fr.qilat.prisonrp.server.network.packets;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public enum PacketAction {
    GETLIST,
    UPDATEONE
}
