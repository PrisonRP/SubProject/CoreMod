package fr.qilat.prisonrp.client.gui;

import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.client.gui.component.button.GuiBttJob;
import fr.qilat.prisonrp.server.game.jobs.Job;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by GartoxFR on 17/12/2017.
 */
@SideOnly(Side.CLIENT)
public class GuiJobSelector extends GuiScreen {

    private boolean isInit = false;
    private static ResourceLocation DEFAULT_BACKGROUND;
    private GuiBttJob bttGardien, bttGouverneur, bttPrisonnier;
    private int bbtId = 0;

    static {
        DEFAULT_BACKGROUND = new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/defaultbackground.png");
    }

    @Override
    public void initGui() {
        isInit = true;
        bttGardien = new GuiBttJob(this, Job.getById(0),bbtId++,20, 20);
        bttGouverneur = new GuiBttJob(this, Job.getById(1),bbtId++,45, 20);
        bttPrisonnier = new GuiBttJob(this, Job.getById(2),bbtId++,70, 20);

        this.addButton(bttGardien);
        this.addButton(bttGouverneur);
        this.addButton(bttPrisonnier);
    }

    public void defaultBackground() {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        if(this.mc != null)
            this.mc.getTextureManager().bindTexture(DEFAULT_BACKGROUND);
        Gui.drawScaledCustomSizeModalRect(this.width / 2 - 256 /2, this.height / 2 - 256, 0, 0, 1, 1, 256, 256, 1,1);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        GuiUtils.drawHoveringText(this.hoveredText, mouseX, mouseY, Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight, 150, Minecraft.getMinecraft().fontRendererObj);
    }
}
