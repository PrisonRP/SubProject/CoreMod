package fr.qilat.prisonrp.client.gui.component.button;

import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.client.gui.GuiScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Qilat on 09/12/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.CLIENT)
public abstract class GuiBttMove extends GuiButton {
    GuiScreen owner;

    GuiBttMove(GuiScreen owner, int buttonId, int x, int y, int width, int height) {
        super(buttonId, x, y, width, height, "");
        this.owner = owner;
        this.visible = true;
        this.enabled = true;
    }

    /**
     * Draws this button to the screen.
     */
    public abstract void drawButton(Minecraft mc, int mouseX, int mouseY);

    public static class Up extends GuiBttMove {
        public Up(GuiScreen owner, int buttonId, int x, int y, int width, int height) {
            super(owner, buttonId, x, y, width, height);
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY) {
            if (this.visible) {
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                this.hovered = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;

                if (this.hovered) {
                    this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/arrowuphover.png"));
                }else{
                    this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/arrowup.png"));
                }
                Gui.drawScaledCustomSizeModalRect(this.xPosition, this.yPosition, 0, 0, 1, 1, this.width, this.height, 1, 1);
            }
        }
    }

    public static class Down extends GuiBttMove {
        public Down (GuiScreen owner,int buttonId, int x, int y, int width, int height) {
            super(owner, buttonId, x, y, width, height);
        }

        @Override
        public void drawButton(Minecraft mc, int mouseX, int mouseY) {
            if (this.visible) {
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                this.hovered = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;

                if (this.hovered) {
                    this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/arrowdownhover.png"));
                }else{
                    this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/arrowdown.png"));
                }
                Gui.drawScaledCustomSizeModalRect(this.xPosition, this.yPosition, 0, 0, 1, 1, this.width, this.height, 1, 1);
            }
        }
    }
}