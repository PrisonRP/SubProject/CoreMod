package fr.qilat.prisonrp.client.gui;

import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.client.gui.component.GuiLocEntry;
import fr.qilat.prisonrp.client.gui.component.button.GuiBttMove;
import fr.qilat.prisonrp.server.network.handler.DropZoneNetworkHandler;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiUtils;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.io.IOException;
import java.util.List;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public class GuiDropZone extends GuiScreen {
    private static final String ROOT_DIRECTORY = "textures/gui/safezone/";
    private static ResourceLocation DEFAULT_BACKGROUND;
    private static int firstIDShown = -1;
    private static int oldIDShown;
    private static int insideLeft;
    private static int insideTop;
    private static int entryWidth = 260;
    private static int entryHeight = 50;
    private static int amountShown = 4;
    private static int idButton = 0;

    static {
        DEFAULT_BACKGROUND = new ResourceLocation(PrisonRPCore.MODID, ROOT_DIRECTORY + "defaultbackground.png");
    }

    private GuiLocEntry firstEntry;
    private GuiLocEntry secondEntry;
    private GuiLocEntry thirdEntry;
    private GuiLocEntry fourthEntry;

    private GuiButton upButton;
    private GuiButton downButton;

    private List<String> hoveredText = null;
    private boolean isInit = false;

    @Override
    public void initGui() {
        isInit = false;
        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();

        if (!DropZoneNetworkHandler.get().getZones().isEmpty() && firstIDShown == -1) {
            firstIDShown = 0;
        }

        int arrowWidth = 25;
        int arrowHeight = 25;
        int arrowX = insideLeft + 256 * 81 / 100;
        int upArrowY = this.height * 42 / 100;
        int downArrowY = this.height * 103 / 200;

        upButton = new GuiBttMove.Up(this, idButton++, arrowX, upArrowY, arrowWidth, arrowHeight);
        downButton = new GuiBttMove.Down(this, idButton++, arrowX, downArrowY, arrowWidth, arrowHeight);
        this.addButton(upButton);
        this.addButton(downButton);


        insideLeft = this.width / 2 - GuiDropZone.entryWidth / 2 + this.width * 2 / 100;
        insideTop = this.height / 2 - 256 / 2;

        int topPadding = 256 * 3 / 100;
        int yPos = insideTop + topPadding + 2;
        this.firstEntry = new GuiLocEntry(this, insideLeft, yPos, entryWidth + 10, entryHeight - 17);
        this.secondEntry = new GuiLocEntry(this, insideLeft, yPos += GuiDropZone.entryHeight + topPadding, entryWidth + 10, entryHeight - 17);
        this.thirdEntry = new GuiLocEntry(this, insideLeft, yPos += GuiDropZone.entryHeight + topPadding, entryWidth + 10, entryHeight - 17);
        this.fourthEntry = new GuiLocEntry(this, insideLeft, yPos += GuiDropZone.entryHeight + topPadding, entryWidth + 10, entryHeight - 17);

        this.firstEntry.initGui(idButton++, idButton++, idButton++);
        this.secondEntry.initGui(idButton++, idButton++, idButton++);
        this.thirdEntry.initGui(idButton++, idButton++, idButton++);
        this.fourthEntry.initGui(idButton++, idButton++, idButton++);

        isInit = true;
    }

    public <T extends GuiButton> T addButton(T button) {
        return super.addButton(button);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();

        if (!DropZoneNetworkHandler.get().getZones().isEmpty()
                && GuiDropZone.firstIDShown != -1
                && isInit) {
            this.firstEntry.setZone(DropZoneNetworkHandler.get().getZones().get(GuiDropZone.firstIDShown));
            this.secondEntry.setZone(DropZoneNetworkHandler.get().getZones().size() >= (GuiDropZone.firstIDShown + 2) ? DropZoneNetworkHandler.get().getZones().get(GuiDropZone.firstIDShown + 1) : null);
            this.thirdEntry.setZone(DropZoneNetworkHandler.get().getZones().size() >= (GuiDropZone.firstIDShown + 3) ? DropZoneNetworkHandler.get().getZones().get(GuiDropZone.firstIDShown + 2) : null);
            this.fourthEntry.setZone(DropZoneNetworkHandler.get().getZones().size() >= (GuiDropZone.firstIDShown + 4) ? DropZoneNetworkHandler.get().getZones().get(GuiDropZone.firstIDShown + 3) : null);

            this.drawEntries();

            this.upButton.visible = firstIDShown != 0;
            this.downButton.visible = firstIDShown + amountShown != DropZoneNetworkHandler.get().getZones().size() && DropZoneNetworkHandler.get().getZones().size() > amountShown;

            this.hoveredText = null;
            super.drawScreen(mouseX, mouseY, partialTicks);

            this.mc.fontRendererObj.drawString(
                    DropZoneNetworkHandler.get().countZone() + " safezone" + (DropZoneNetworkHandler.get().countZone() > 1 ? "s" : "") + " : " +
                            DropZoneNetworkHandler.get().countZone(Zone.State.ENABLE) + " activée" + (DropZoneNetworkHandler.get().countZone(Zone.State.ENABLE) > 1 ? "s" : "") + " / " +
                            DropZoneNetworkHandler.get().countZone(Zone.State.DISABLE) + " désactivée" + (DropZoneNetworkHandler.get().countZone(Zone.State.DISABLE) > 1 ? "s" : ""),
                    this.width / 2 - 256 / 2 + 22, this.height * 153 / 200, new Color(200, 0, 0).getRGB());

        } else {
            this.mc.fontRendererObj.drawSplitString("Aucune point de drop existant sur ce serveur", this.width / 2 - 256 / 2 + 30, this.height / 2 - 20, 200, new Color(0, 100, 200).getRGB());
        }
        this.mc.fontRendererObj.drawString("Liste des points de drop ", this.width / 2 - 256 / 2 + 22, insideTop + 7 , new Color(100, 100, 100).getRGB());

        if (this.hoveredText != null && !this.hoveredText.isEmpty())
            GuiUtils.drawHoveringText(this.hoveredText, mouseX, mouseY, Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight, 150, Minecraft.getMinecraft().fontRendererObj);

    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);

        this.firstEntry.textboxKeyTyped(typedChar, keyCode);
        this.secondEntry.textboxKeyTyped(typedChar, keyCode);
        this.thirdEntry.textboxKeyTyped(typedChar, keyCode);
        this.fourthEntry.textboxKeyTyped(typedChar, keyCode);

        if (keyCode == 15) {
            if (this.firstEntry.isFocused()) this.firstEntry.nextFocus();
            if (this.secondEntry.isFocused()) this.secondEntry.nextFocus();
            if (this.thirdEntry.isFocused()) this.thirdEntry.nextFocus();
            if (this.fourthEntry.isFocused()) this.fourthEntry.nextFocus();
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.firstEntry.mouseClicked(mouseX, mouseY, mouseButton);
        this.secondEntry.mouseClicked(mouseX, mouseY, mouseButton);
        this.thirdEntry.mouseClicked(mouseX, mouseY, mouseButton);
        this.fourthEntry.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if (button.id == this.upButton.id) {
            if (firstIDShown != 0 && DropZoneNetworkHandler.get().getZones().size() > amountShown) {
                oldIDShown = firstIDShown;
                firstIDShown--;
            }
        } else if (button.id == this.downButton.id) {
            if (firstIDShown + amountShown != DropZoneNetworkHandler.get().getZones().size()) {
                oldIDShown = firstIDShown;
                firstIDShown++;
            }
        } else {
            this.handleClick(button.id);
        }
    }

    private void handleClick(int idButton) {
        if (this.firstEntry != null)
            this.firstEntry.handleClick(idButton);
        if (this.secondEntry != null)
            this.secondEntry.handleClick(idButton);
        if (this.thirdEntry != null)
            this.thirdEntry.handleClick(idButton);
        if (this.fourthEntry != null)
            this.fourthEntry.handleClick(idButton);
    }

    private void drawEntries() {
        if (this.firstEntry != null)
            this.firstEntry.drawEntry();
        if (this.secondEntry != null)
            this.secondEntry.drawEntry();
        if (this.thirdEntry != null)
            this.thirdEntry.drawEntry();
        if (this.fourthEntry != null)
            this.fourthEntry.drawEntry();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.updateCursorPos();
    }

    private void updateCursorPos() {
        if (this.firstEntry != null)
            this.firstEntry.updateCursorPos();
        if (this.secondEntry != null)
            this.secondEntry.updateCursorPos();
        if (this.thirdEntry != null)
            this.thirdEntry.updateCursorPos();
        if (this.fourthEntry != null)
            this.fourthEntry.updateCursorPos();
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    public void drawDefaultBackground() {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        if (this.mc != null)
            this.mc.getTextureManager().bindTexture(DEFAULT_BACKGROUND);
        Gui.drawScaledCustomSizeModalRect(this.width / 2 - 256 / 2, this.height / 2 - 256 / 2, 0, 0, 1, 1, 256, 256, 1, 1);
    }

    public void printHoveredText(List<String> list) {
        this.hoveredText = list;
    }
}

