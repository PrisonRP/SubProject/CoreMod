package fr.qilat.prisonrp.client.gui.component.button;

import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.client.gui.GuiScreen;
import fr.qilat.prisonrp.server.game.jobs.Job;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

import java.util.Arrays;

/**
 * Created by GartoxFR on 17/12/2017.
 */
public class GuiBttJob extends GuiButton{

    private Job job;
    private GuiScreen owner;

    public GuiBttJob(GuiScreen owner, Job job,int id, int x, int y) {
        super(id, x, y, 22, 22, "");
        this.owner = owner;
        this.job = job;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if(this.visible) {
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.hovered = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;

            if(this.hovered) {
                this.owner.printHoveredText(Arrays.asList(job.getName(), "Cliquez pour devenir" + job.getName()));
                this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/utils/button/btt_hover.png"));
            } else {
                this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/utils/button/btt.png"));
            }
            Gui.drawScaledCustomSizeModalRect(this.xPosition, this.yPosition, 0, 0, 1, 1, 22, 22, 1, 1);

            this.owner.mc.getTextureManager().bindTexture(job.getIcon());
            Gui.drawScaledCustomSizeModalRect(this.xPosition + 2, this.yPosition + 2, 0, 0, 1, 1, 18, 18, 1, 1);
        }
    }


}
