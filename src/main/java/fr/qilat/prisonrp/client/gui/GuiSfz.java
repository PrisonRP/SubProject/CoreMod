package fr.qilat.prisonrp.client.gui;

import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.client.gui.component.button.GuiBttMove;
import fr.qilat.prisonrp.client.gui.component.GuiZoneEntry;
import fr.qilat.prisonrp.server.network.handler.SafeZoneNetworkHandler;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.io.IOException;

/**
 * Created by Qilat on 08/12/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.CLIENT)
public class GuiSfz extends GuiScreen {
    private static final String ROOT_DIRECTORY = "textures/gui/safezone/";
    private static ResourceLocation DEFAULT_BACKGROUND;
    private static int firstIDShown = -1;
    private static int oldIDShown;
    private static int insideLeft = 0;
    private static int insideTop = 0;
    private static int entryWidth = 260;
    private static int entryHeight = 50;
    private static int amountShown = 4;
    private static int idButton = 0;

    static {
        DEFAULT_BACKGROUND = new ResourceLocation(PrisonRPCore.MODID, ROOT_DIRECTORY + "defaultbackground.png");
    }

    private GuiZoneEntry firstEntry;
    private GuiZoneEntry secondEntry;
    private GuiZoneEntry thirdEntry;
    private GuiZoneEntry fourthEntry;

    private GuiButton upButton;
    private GuiButton downButton;

    private boolean isInit = false;

    @Override
    public void initGui() {
        isInit = false;
        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();

        if (!SafeZoneNetworkHandler.get().getZones().isEmpty() && firstIDShown == -1) {
            firstIDShown = 0;
        }

        int arrowWidth = 25;
        int arrowHeight = 25;
        int arrowX = insideLeft + 256 * 81 / 100;
        int upArrowY = this.height * 42 / 100;
        int downArrowY = this.height * 103 / 200;

        upButton = new GuiBttMove.Up(this, idButton++, arrowX, upArrowY, arrowWidth, arrowHeight);
        downButton = new GuiBttMove.Down(this, idButton++, arrowX, downArrowY, arrowWidth, arrowHeight);
        this.addButton(upButton);
        this.addButton(downButton);


        insideLeft = this.width / 2 - GuiSfz.entryWidth / 2 + this.width * 2/100;
        insideTop = this.height / 2 - 256 / 2;

        int topPadding = 256 * 3 / 100;
        int yPos = insideTop + topPadding + 2;
        this.firstEntry = new GuiZoneEntry(SafeZoneNetworkHandler.get(), this, insideLeft, yPos, entryWidth, entryHeight);
        this.secondEntry = new GuiZoneEntry(SafeZoneNetworkHandler.get(),this, insideLeft, yPos += GuiSfz.entryHeight + topPadding, entryWidth, entryHeight);
        this.thirdEntry = new GuiZoneEntry(SafeZoneNetworkHandler.get(),this, insideLeft, yPos += GuiSfz.entryHeight + topPadding, entryWidth, entryHeight);
        this.fourthEntry = new GuiZoneEntry(SafeZoneNetworkHandler.get(),this, insideLeft, yPos += GuiSfz.entryHeight + topPadding, entryWidth, entryHeight);

        this.firstEntry.initGui(idButton++, idButton++, idButton++);
        this.secondEntry.initGui(idButton++, idButton++, idButton++);
        this.thirdEntry.initGui(idButton++, idButton++, idButton++);
        this.fourthEntry.initGui(idButton++, idButton++, idButton++);

        isInit = true;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();

        if (!SafeZoneNetworkHandler.get().getZones().isEmpty()
                && GuiSfz.firstIDShown != -1
                && isInit) {
            this.firstEntry.setZone(SafeZoneNetworkHandler.get().getZones().get(GuiSfz.firstIDShown));
            this.secondEntry.setZone(SafeZoneNetworkHandler.get().getZones().size() >= (GuiSfz.firstIDShown + 2) ? SafeZoneNetworkHandler.get().getZones().get(GuiSfz.firstIDShown + 1) : null);
            this.thirdEntry.setZone(SafeZoneNetworkHandler.get().getZones().size() >= (GuiSfz.firstIDShown + 3) ? SafeZoneNetworkHandler.get().getZones().get(GuiSfz.firstIDShown + 2) : null);
            this.fourthEntry.setZone(SafeZoneNetworkHandler.get().getZones().size() >= (GuiSfz.firstIDShown + 4) ? SafeZoneNetworkHandler.get().getZones().get(GuiSfz.firstIDShown + 3) : null);

            this.drawEntries();

            this.upButton.visible = firstIDShown != 0;
            this.downButton.visible = firstIDShown + amountShown != SafeZoneNetworkHandler.get().getZones().size() && SafeZoneNetworkHandler.get().getZones().size() > amountShown;

            this.hoveredText = null;
            super.drawScreen(mouseX, mouseY, partialTicks);

            this.mc.fontRendererObj.drawString(
                    SafeZoneNetworkHandler.get().countZone() + " safezone" + (SafeZoneNetworkHandler.get().countZone() > 1 ? "s" : "") + " : " +
                            SafeZoneNetworkHandler.get().countZone(Zone.State.ENABLE) + " activée" + (SafeZoneNetworkHandler.get().countZone(Zone.State.ENABLE) > 1 ? "s" : "") + " / " +
                            SafeZoneNetworkHandler.get().countZone(Zone.State.DISABLE) + " désactivée" + (SafeZoneNetworkHandler.get().countZone(Zone.State.DISABLE) > 1 ? "s" : ""),
                    this.width / 2 - 256 / 2 + 22, this.height * 153/200 , new Color(200, 0, 0).getRGB());

        } else {
            if(this.mc != null)
                this.drawString(this.mc.fontRendererObj, "Aucune safezone existante sur ce serveur", this.width / 2 - 256 / 2 + 15, this.height / 2 - 15, new Color(0, 100, 200).getRGB());

        }
        this.mc.fontRendererObj.drawString("Liste des safezones", this.width / 2 - 256 / 2 + 22, insideTop + 7 , new Color(100, 100, 100).getRGB());

        if(this.hoveredText != null && !this.hoveredText.isEmpty())
            GuiUtils.drawHoveringText(this.hoveredText , mouseX, mouseY, Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight, 150, Minecraft.getMinecraft().fontRendererObj);

    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);

        this.firstEntry.textboxKeyTyped(typedChar, keyCode);
        this.secondEntry.textboxKeyTyped(typedChar, keyCode);
        this.thirdEntry.textboxKeyTyped(typedChar, keyCode);
        this.fourthEntry.textboxKeyTyped(typedChar, keyCode);

        if (keyCode == 15) {
            if (this.firstEntry.isFocused()) this.firstEntry.nextFocus();
            if (this.secondEntry.isFocused()) this.secondEntry.nextFocus();
            if (this.thirdEntry.isFocused()) this.thirdEntry.nextFocus();
            if (this.fourthEntry.isFocused()) this.fourthEntry.nextFocus();
        }

        if (keyCode == 28 || keyCode == 156) {
            //TODO SAVE
        }
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.firstEntry.mouseClicked(mouseX, mouseY, mouseButton);
        this.secondEntry.mouseClicked(mouseX, mouseY, mouseButton);
        this.thirdEntry.mouseClicked(mouseX, mouseY, mouseButton);
        this.fourthEntry.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        if(button.id == this.upButton.id) {
            if (firstIDShown != 0 && SafeZoneNetworkHandler.get().getZones().size() > amountShown) {
                oldIDShown = firstIDShown;
                firstIDShown--;
            }
        }else if(button.id == this.downButton.id) {
            if (firstIDShown + amountShown != SafeZoneNetworkHandler.get().getZones().size()) {
                oldIDShown = firstIDShown;
                firstIDShown++;
            }
        } else{
            this.handleClick(button.id);
        }
    }

    private void handleClick(int idButton){
        if (this.firstEntry != null)
            this.firstEntry.handleClick(idButton);
        if (this.secondEntry != null)
            this.secondEntry.handleClick(idButton);
        if (this.thirdEntry != null)
            this.thirdEntry.handleClick(idButton);
        if (this.fourthEntry != null)
            this.fourthEntry.handleClick(idButton);
    }

    private void drawEntries() {
        if (this.firstEntry != null)
            this.firstEntry.drawEntry();
        if (this.secondEntry != null)
            this.secondEntry.drawEntry();
        if (this.thirdEntry != null)
            this.thirdEntry.drawEntry();
        if (this.fourthEntry != null)
            this.fourthEntry.drawEntry();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        this.updateCursorPos();
    }

    private void updateCursorPos() {
        if (this.firstEntry != null)
            this.firstEntry.updateCursorPos();
        if (this.secondEntry != null)
            this.secondEntry.updateCursorPos();
        if (this.thirdEntry != null)
            this.thirdEntry.updateCursorPos();
        if (this.fourthEntry != null)
            this.fourthEntry.updateCursorPos();
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    public void drawDefaultBackground() {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        if(this.mc != null)
            this.mc.getTextureManager().bindTexture(DEFAULT_BACKGROUND);
        Gui.drawScaledCustomSizeModalRect(this.width / 2 - 256 / 2, this.height / 2 - 256 / 2, 0, 0, 1, 1, 256, 256, 1, 1);
    }


}