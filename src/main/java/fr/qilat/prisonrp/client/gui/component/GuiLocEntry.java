package fr.qilat.prisonrp.client.gui.component;

import fr.qilat.prisonrp.client.gui.GuiScreen;
import fr.qilat.prisonrp.client.gui.component.button.GuiBttDelZone;
import fr.qilat.prisonrp.client.gui.component.button.GuiBttSaveZone;
import fr.qilat.prisonrp.client.gui.component.button.GuiBttStateZone;
import fr.qilat.prisonrp.server.network.handler.DropZoneNetworkHandler;
import fr.qilat.prisonrp.server.network.handler.SafeZoneNetworkHandler;
import fr.qilat.prisonrp.server.utils.Utils;
import fr.qilat.prisonrp.server.utils.Zone;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.awt.*;

import static fr.qilat.prisonrp.server.utils.Zone.State.DELETED;
import static fr.qilat.prisonrp.server.utils.Zone.State.DISABLE;
import static fr.qilat.prisonrp.server.utils.Zone.State.ENABLE;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
@SideOnly(Side.CLIENT)
public class GuiLocEntry {

    private static int fontHeight = Minecraft.getMinecraft().fontRendererObj.FONT_HEIGHT;

    private int baseX;
    private int baseY;
    private int listWidth;
    private int slotHeight;

    private int oldId = -1;
    private GuiScreen owner;
    private Minecraft mc;
    private Zone zone;

    private GuiTextField name;
    private GuiTextField pos1x;
    private GuiTextField pos1y;
    private GuiTextField pos1z;

    private GuiBttSaveZone buttonSaveSFZ;
    private GuiBttStateZone buttonStateSFZ;
    private GuiBttDelZone buttonDelSFZ;

    public GuiLocEntry(GuiScreen owner, int x, int y, int listWidth, int slotHeight) {
        this.owner = owner;
        this.mc = Minecraft.getMinecraft();
        this.baseX = x + 5;
        this.baseY = y + 6;
        this.listWidth = listWidth;
        this.slotHeight = slotHeight;

    }

    public void initGui(int buttonSaveID, int buttonStateID, int buttonDelID) {
        int intervalY = 4;
        int intervalX = 4;
        int fieldHeight = 10;
        int fieldWidth = 40;
        int baseX = this.baseX + 2;
        int baseY = this.baseY + 7;

        this.name = new GuiTextField(6, this.mc.fontRendererObj, baseX + 48, baseY - 1, fieldWidth * 17 / 8 + 1, fieldHeight); // 1/8
        this.pos1x = new GuiTextField(0, this.mc.fontRendererObj, baseX + 0 * fieldWidth + 1 * intervalX, baseY + fontHeight + intervalY, fieldWidth, fieldHeight);
        this.pos1y = new GuiTextField(1, this.mc.fontRendererObj, baseX + 1 * fieldWidth + 2 * intervalX, baseY + fontHeight + intervalY, fieldWidth, fieldHeight);
        this.pos1z = new GuiTextField(2, this.mc.fontRendererObj, baseX + 2 * fieldWidth + 3 * intervalX, baseY + fontHeight + intervalY, fieldWidth + 2, fieldHeight);

        baseX += 2;
        baseY -= 2;
        this.buttonSaveSFZ = new GuiBttSaveZone(this.owner, buttonSaveID, baseX + 3 * fieldWidth + 4 * intervalX, baseY + 2);
        this.buttonStateSFZ = new GuiBttStateZone(this.owner, buttonStateID, baseX + 3 * fieldWidth + 4 * intervalX + 21, baseY + 2);
        this.buttonDelSFZ = new GuiBttDelZone(this.owner, buttonDelID,baseX + 3 * fieldWidth + 4 * intervalX + 21 * 2, baseY + 2);

        this.owner.addButton(this.buttonSaveSFZ);
        this.owner.addButton(this.buttonStateSFZ);
        this.owner.addButton(this.buttonDelSFZ);

    }

    private void setTextFiledContent() {
        this.name.setText(getZone().getWorldName());

        this.pos1x.setText(String.valueOf(getZone().getPos1X()));
        this.pos1y.setText(String.valueOf(getZone().getPos1Y()));
        this.pos1z.setText(String.valueOf(getZone().getPos1Z()));
    }

    private void drawTextField() {
        if (this.oldId != getZone().getId()) {
            this.setTextFiledContent();
            this.oldId = this.getZone().getId();
        }

        this.name.drawTextBox();

        this.pos1x.drawTextBox();
        this.pos1y.drawTextBox();
        this.pos1z.drawTextBox();
    }

    public void drawEntry() {
        if (this.getZone() != null) {
            int rectHeight = slotHeight;
            int rectWidth = listWidth * 75 / 100;

            this.mc.getTextureManager().bindTexture(this.getZone().getState().getBackground());
            Gui.drawScaledCustomSizeModalRect(this.baseX, this.baseY + 2, 0, 0, 1, 1, rectWidth, rectHeight - 1, 1, 1);

            this.mc.fontRendererObj.drawString("ID n°" + Integer.toString(this.zone.getId()), baseX + 7, baseY + 5, new Color(255, 255, 255).getRGB());
            this.drawTextField();
            this.updateButtonHoveringText();
            this.showButton();
        } else {
            this.hideButton();
        }
    }

    private void updateButtonHoveringText() {
        switch (this.zone.getState()){
            case ENABLE:
                this.buttonStateSFZ.setPathHoveringText(DISABLE.getPathName());
                this.buttonDelSFZ.setPathHoveringText(DELETED.getPathName());
                break;
            case DISABLE:
                this.buttonStateSFZ.setPathHoveringText(ENABLE.getPathName());
                this.buttonDelSFZ.setPathHoveringText(DELETED.getPathName());
                break;
            case DELETED:
                this.buttonStateSFZ.setPathHoveringText("prisonrp.btt.statesfz.tobedelete");
                this.buttonDelSFZ.setPathHoveringText("prisonrp.btt.statesfz.canceldelete");
                break;
        }
    }

    private void showButton() {
        this.buttonSaveSFZ.visible = true;
        this.buttonStateSFZ.visible = true;
        this.buttonDelSFZ.visible = true;
    }

    private void hideButton() {
        this.buttonSaveSFZ.visible = false;
        this.buttonStateSFZ.visible = false;
        this.buttonDelSFZ.visible = false;
    }

    public void updateCursorPos() {
        if (this.name != null)
            this.name.updateCursorCounter();

        if (this.pos1x != null)
            this.pos1x.updateCursorCounter();
        if (this.pos1y != null)
            this.pos1y.updateCursorCounter();
        if (this.pos1z != null)
            this.pos1z.updateCursorCounter();
    }

    public Zone getZone() {
        return this.zone;
    }

    public void setZone(Zone zone) {
        if (zone == null) {
            this.zone = null;
            return;
        }
        if (this.oldId != zone.getId())
            this.zone = zone;
    }

    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        this.name.mouseClicked(mouseX, mouseY, mouseButton);
        this.pos1x.mouseClicked(mouseX, mouseY, mouseButton);
        this.pos1y.mouseClicked(mouseX, mouseY, mouseButton);
        this.pos1z.mouseClicked(mouseX, mouseY, mouseButton);
    }

    public void textboxKeyTyped(char typedChar, int keyCode) {
        this.name.textboxKeyTyped(typedChar, keyCode);
        this.pos1x.textboxKeyTyped(typedChar, keyCode);
        this.pos1y.textboxKeyTyped(typedChar, keyCode);
        this.pos1z.textboxKeyTyped(typedChar, keyCode);
    }

    public boolean isFocused() {
        return this.name.isFocused()
                || this.pos1x.isFocused()
                || this.pos1y.isFocused()
                || this.pos1z.isFocused();
    }

    public void nextFocus() {
        if (this.name.isFocused()) {
            this.name.setFocused(false);
            this.pos1x.setFocused(true);
        } else if (this.pos1x.isFocused()) {
            this.pos1x.setFocused(false);
            this.pos1y.setFocused(true);
        } else if (this.pos1y.isFocused()) {
            this.pos1y.setFocused(false);
            this.pos1z.setFocused(true);
        } else if (this.pos1z.isFocused()) {
            this.pos1z.setFocused(false);
            this.name.setFocused(true);
        }
    }

    public void handleClick(int idButton) {
        if(this.buttonSaveSFZ.id == idButton){
            DropZoneNetworkHandler.get().saveZone(getNewSafeZone());
            if(this.zone.getState().equals(DELETED))
                DropZoneNetworkHandler.get().deleteZone(zone);
        }else if(this.buttonStateSFZ.id == idButton){
            if(!this.zone.getState().equals(DELETED))
                this.zone.setState(this.zone.getState().equals(ENABLE) ? DISABLE : (this.zone.getState().equals(DISABLE) ? ENABLE : DELETED));
        }else if(this.buttonDelSFZ.id == idButton){
            this.zone.setState(this.zone.getState().equals(DELETED) ? ENABLE : DELETED);
        }
    }

    public Zone getNewSafeZone(){
        this.zone.setPos1(new BlockPos(Utils.parseInt(this.pos1x.getText()), Utils.parseInt(this.pos1y.getText()), Utils.parseInt(this.pos1z.getText())));
        this.zone.setWorldName(this.name.getText());
        return this.zone;
    }
}
