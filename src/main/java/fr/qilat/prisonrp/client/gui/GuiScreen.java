package fr.qilat.prisonrp.client.gui;

import net.minecraft.client.gui.GuiButton;

import java.util.List;

/**
 * Created by Qilat on 17/12/2017 for PrisonRP.
 */
public abstract class GuiScreen extends net.minecraft.client.gui.GuiScreen{
    List<String> hoveredText = null;

    public <T extends GuiButton> T addButton(T button) {
        return super.addButton(button);
    }

    public void printHoveredText(List<String> list){
        this.hoveredText = list;
    }

}
