package fr.qilat.prisonrp.client.gui.component.button;

import fr.qilat.prisonrp.PrisonRPCore;
import fr.qilat.prisonrp.client.gui.GuiScreen;
import fr.qilat.prisonrp.client.gui.GuiSfz;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import scala.actors.threadpool.Arrays;

/**
 * Created by Qilat on 09/12/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.CLIENT)
public class GuiBttSaveZone extends GuiButton {

    private GuiScreen owner;

    public GuiBttSaveZone(GuiScreen owner, int buttonId, int x, int y) {
        super(buttonId, x, y, 22, 22, "");
        this.owner = owner;
        this.visible = true;
        this.enabled = true;
    }

    /**
     * Draws this button to the screen.
     */
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if (this.visible) {
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;

            if (this.hovered) {
                this.owner.printHoveredText(Arrays.asList(new String[]{I18n.format("prisonrp.btt.savesfz")}));
                this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/hoveredsavebtt.png"));
            }else{
                this.owner.mc.getTextureManager().bindTexture(new ResourceLocation(PrisonRPCore.MODID, "textures/gui/safezone/savebtt.png"));
            }
            Gui.drawScaledCustomSizeModalRect(this.xPosition, this.yPosition, 0, 0, 1, 1, 18, 18, 1, 1);

        }
    }
}
