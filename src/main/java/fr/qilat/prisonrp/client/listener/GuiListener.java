package fr.qilat.prisonrp.client.listener;

import fr.qilat.prisonrp.client.PRPClient;
import fr.qilat.prisonrp.client.gui.GuiCustomInGameMenu;
import fr.qilat.prisonrp.client.gui.GuiCustomInventory;
import fr.qilat.prisonrp.client.gui.GuiCustomMainMenu;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.input.Mouse;

/**
 * Created by Qilat on 14/11/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SuppressWarnings("unused")
@Mod.EventBusSubscriber(value = {Side.CLIENT})
public class GuiListener {

    private static boolean authorizeMultiGui = true; //TODO true if staff

    public static final Minecraft MC = Minecraft.getMinecraft();

    @SubscribeEvent
    public static void onOpenGui(GuiOpenEvent event) {
        if (event.getGui() != null) {
            if (event.getGui().getClass() == GuiMainMenu.class
                    || (event.getGui().getClass() == GuiMultiplayer.class && !authorizeMultiGui)) {
                event.setGui(new GuiCustomMainMenu());
            } else if (event.getGui().getClass() == GuiIngameMenu.class) {
                event.setGui(new GuiCustomInGameMenu());
            } else if (event.getGui().getClass() == GuiInventory.class) {
                event.setGui(new GuiCustomInventory(Minecraft.getMinecraft().player));
            }
        }
    }

    @SubscribeEvent
    public static void onPreRendering(RenderGameOverlayEvent.Pre event){
        if(event.getType().equals(RenderGameOverlayEvent.ElementType.CHAT))
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void onChatting(ClientChatReceivedEvent event){
        PRPClient.getGuiChat().printChatMessage(event.getMessage());
        System.out.println("send");
    }

    @SubscribeEvent
    public static void onPostRendering(RenderGameOverlayEvent.Post event){
        ScaledResolution scaledResolution = new ScaledResolution(MC);
        int width = scaledResolution.getScaledWidth();
        int height = scaledResolution.getScaledHeight();
        int mouseX = Mouse.getX() * width / MC.displayWidth;
        int mouseZ = height - Mouse.getY() * height / MC.displayHeight - 1;

        if(event.getType().equals(RenderGameOverlayEvent.ElementType.ALL)) {
            GlStateManager.pushMatrix();
            PRPClient.getGuiChat().drawChat(MC.ingameGUI.getUpdateCounter());
            GlStateManager.popMatrix();

        }
    }


}
