package fr.qilat.prisonrp.client.listener;

import fr.qilat.prisonrp.PrisonRPCore;

import fr.qilat.prisonrp.server.network.handler.DropZoneNetworkHandler;

import fr.qilat.prisonrp.client.gui.GuiJobSelector;

import fr.qilat.prisonrp.server.network.handler.QuaranZoneNetworkHandler;
import fr.qilat.prisonrp.server.network.handler.SafeZoneNetworkHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

/**
 * Created by Qilat on 06/12/2017 for forge-1.10.2-12.18.3.2511-mdk.
 */
@SideOnly(Side.CLIENT)
public class InputListener {
    private static InputListener instance = null;

    private static KeyBinding keyBindSafeZone;
    private static KeyBinding keyBindQuaranZone;
    private static KeyBinding keyBindDropZone;

    private static KeyBinding keyBindingJobSelector;



    public InputListener() {
        if (InputListener.instance == null) {
            instance = this;

            keyBindSafeZone = new KeyBinding(PrisonRPCore.MODID + ".key.safezone", Keyboard.KEY_SEMICOLON, "key.categories.inventory");
            ClientRegistry.registerKeyBinding(keyBindSafeZone);
            keyBindQuaranZone = new KeyBinding(PrisonRPCore.MODID + ".key.quaranzone", Keyboard.KEY_APOSTROPHE, "key.categories.inventory");
            ClientRegistry.registerKeyBinding(keyBindQuaranZone);

            keyBindDropZone = new KeyBinding(PrisonRPCore.MODID + ".key.dropzone", Keyboard.KEY_BACKSLASH, "key.categories.inventory");
            ClientRegistry.registerKeyBinding(keyBindDropZone);

            keyBindingJobSelector = new KeyBinding(PrisonRPCore.MODID + ".key.jobselector", Keyboard.KEY_Y, "key.categories.inventory");
            ClientRegistry.registerKeyBinding(keyBindingJobSelector);


            MinecraftForge.EVENT_BUS.register(this);
        }
    }

    @SubscribeEvent
    public void onEvent(InputEvent.KeyInputEvent event) {
        if (keyBindSafeZone.isPressed()) {
            keySafeZonePressed();
        }
        if(keyBindQuaranZone.isPressed()){
            keyQuaranZonePressed();
        }

        if(keyBindDropZone.isPressed()){
            keyDropZonePressed();
        }
        if(keyBindingJobSelector.isPressed()) {
            keyJobSelectorPressed();
        }
    }

    private void keyDropZonePressed() {
        Minecraft.getMinecraft().player.sendChatMessage("Récupéreration des informations concernant les dropzones... Veuillez patienter...");
        DropZoneNetworkHandler.get().loadZones();

        if(keyBindingJobSelector.isPressed())
            keyJobSelectorPressed();
    }

    private void keyJobSelectorPressed() {
        Minecraft.getMinecraft().displayGuiScreen(new GuiJobSelector());

    }

    private void keyQuaranZonePressed() {
        Minecraft.getMinecraft().player.sendChatMessage("Récupéreration des informations concernant les zones de quarantaines... Veuillez patienter...");
        QuaranZoneNetworkHandler.get().loadZones();
    }

    private void keySafeZonePressed() {
        Minecraft.getMinecraft().player.sendChatMessage("Récupéreration des informations concernant les safezones... Veuillez patienter...");
        SafeZoneNetworkHandler.get().loadZones();
    }


}
